from abc import ABC, abstractmethod
from random import random


class Node(ABC):
    """
    A representation of a single board state.
    MCTS works by constructing a tree of these Nodes.
    Could be e.g. a chess or checkers board state.
    """

    @abstractmethod
    def __init__(self, heroes, parent, children):
        # List of possible hereoes beneath, should just be a collection of numbers
        self.heroes = heroes
        # A reference to its parent node
        self.parent = parent
        # A collection of all linked nodes. Is set to None if no children (terminal)
        self.children = children
        # try to keep track of amount of visists
        self.visits = 0

    @abstractmethod
    def find_children(self):
        "All possible successors of this draft state"
        return self.children

    @abstractmethod
    def find_random_child(self):
        "Random successor of this draft state (for more efficient simulation)"
        # TODO write test for random child finder
        return random.sample(self.children, 1)

    @abstractmethod
    def is_terminal(self):
        "Returns True if the node has no children"
        if self.children is None:
            return True
        return False

    @abstractmethod
    def reward(self):
        "Assumes `self` is terminal node. 1=win, 0=loss, .5=tie, etc"
        return 0

    @abstractmethod
    def __hash__(self):
        "Nodes must be hashable"
        return 123456789

    @abstractmethod
    def __eq__(self, other):
        "Nodes must be comparable"

        if not isinstance(other, Node):
            # Other instances can't be equal to this one
            return False
        # TODO make comparable properly
        return True
