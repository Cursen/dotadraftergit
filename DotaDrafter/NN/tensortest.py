import tensorflow as tf
from tensorflow import keras
import numpy as np
import FirebaseMS as firebase
from random import shuffle
import os.path


# Training set, should be a entire draft, and its label. Should be A 75% SPLIT


# test set


class Main:

    def __init__(self):
        self.fdb = firebase.Main()
        self.trainingMatches = []
        self.testingMatches = []
        self.heroes = []
        self.heroLength = 0
        self.save_path = 'C:\\Git\\DotaDrafterGit\\savedModels\\savedNN.ckpt'
        self.model = None

    def main(self):
        self.heroes = self.fdb.firebase_tools.getHeroes()
        self.heroLength = len(self.heroes)
        # self.getMatches117()
        # self.getMatches()
        # self.checkLength()

    def checkLength(self):
        self.fdb.firebase_tools.checkLengths()

    def getMatches(self):
        totalMatches = self.fdb.firebase_tools.createMatches()
        matchDrafts = []
        matchLabels = []
        # each value in total matches should be: [heroId, heroId, heroId, heroId, heroId, heroId], [heroId, heroId,
        # heroId, heroId, heroId, heroId], whoWonLabel?
        for match in totalMatches:
            ma = []
            dire = []
            radiant = []
            radiantWin = match.radiantWin
            for player in match.dire_players:
                dire.append(player.hero_id)

            for player in match.radiant_players:
                radiant.append(player.hero_id)

            ma.append(radiant)
            ma.append(dire)
            print(ma)
            matchDrafts.append(ma)
            if radiantWin:
                matchLabels.append(1)
            else:
                matchLabels.append(0)

        # Now that the "array" is sort off made, turn it into numpy of: train examples. train labels. Test examples.
        # Test labels.
        matchDrafts_np = np.array(matchDrafts)
        matchlabels_np = np.array(matchLabels)
        # the model needs 1 input neuron for each hero. if hero is picked, set neuron to 1 for radiant -1 for dire.
        # rest to 0 hidden layers is less specific, consider 300 or something and test. output is 1 neuron. It should
        # have a value between
        model = keras.Sequential([
            # input layer
            keras.layers.Dense(117),
            # this is the middle layer
            keras.layers.Dense(300, activation=None),
            # look at output to make it between -1 and 1. or 0 to 1
            # sigmoid should do this. Provide documentation for it.
            keras.layers.Dense(1, activation="sigmoid")
        ])

        model.compile(optimizer="adam", metrics=["accuracy"], loss=None)
        model.fit()

    def convertToNeuralId(self, heroId):
        for hero in self.heroes:
            if hero.heroId == heroId:
                return hero.id

    def getEmptyHeroArray(self):
        counter = 0
        heroArray = []
        while counter < len(self.heroes):
            heroArray.append(0)
            counter += 1
        return heroArray

    def split_list(self, alist, wanted_parts):
        length = len(alist)
        return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts]
                for i in range(wanted_parts)]

    def getMatches117(self):
        # all sets are Numpy arrays
        matchDrafts_train, matchLabels_train, matchDrafts_test, matchLabels_test = self.matchSets()

        # the model needs 1 input neuron for each hero. if hero is picked, set neuron to 1 for radiant -1 for dire. rest to 0
        # hidden layers is less specific, consider 300 or something and test.
        # output is 1 neuron. It should have a value between
        model = self.create_model()
        save_dir = os.path.dirname(self.save_path)
        callback = tf.keras.callbacks.ModelCheckpoint(self.save_path, save_weights_only=True, Verbose=1)

        model.fit(matchDrafts_train, matchLabels_train, epochs=300,
                  validation_data=(matchDrafts_test, matchLabels_test), callbacks=[callback])

    def my_func(arg):
        arg = tf.convert_to_tensor(arg, dtype=tf.float32)
        return tf.matmul(arg, arg) + arg

    def matchSets(self):
        totalMatches = self.fdb.firebase_tools.createMatches()
        shuffle(totalMatches)
        matchDrafts = []
        matchLabels = []
        # each value in total matches should be:
        # [1 if radiant, 0 if none -1 if dire], whoWonLabel?
        for match in totalMatches:
            heroArray = self.getEmptyHeroArray()
            radiantWin = match.radiantWin
            for player in match.radiant_players:
                heroArray[self.convertToNeuralId(player.hero_id)] = 1

            for player in match.dire_players:
                heroArray[self.convertToNeuralId(player.hero_id)] = -1
            matchDrafts.append(heroArray)
            if radiantWin:
                matchLabels.append(1)
            else:
                matchLabels.append(0)
        matchDrafts_train, matchDrafts_test = self.split_list(matchDrafts, 2)
        matchLabels_train, matchLabels_test = self.split_list(matchLabels, 2)
        # Now that the "array" is sort off made, turn it into numpy of: train examples. train labels. Test examples.
        # Test labels.
        matchDrafts_trainnp = np.asarray(matchDrafts_train)
        matchLabels_trainnp = np.asarray(matchLabels_train)
        matchDrafts_testnp = np.asarray(matchDrafts_test)
        matchLabels_testnp = np.asarray(matchLabels_test)
        return matchDrafts_trainnp, matchLabels_trainnp, matchDrafts_testnp, matchLabels_testnp

    def loadModel(self):
        model = self.create_model()
        model.load_weights(self.save_path).expect_partial()
        matchDrafts_trainnp, matchLabels_trainnp, matchDrafts_testnp, matchLabels_testnp = self.matchSets()
        loss, acc = model.evaluate(matchDrafts_testnp, matchLabels_testnp)
        print("Restored model, accuracy: {:5.2f}%".format(100 * acc))
        return model

    def check_sets(self):
        result = False

        for x in self.trainingMatches:
            for y in self.testingMatches:
                if x == y:
                    result = True
                    return result
        return result

    def checkDraft(self, radiant, dire):
        heroArray = self.getEmptyHeroArray()
        for pickedHero in radiant:
            heroArray[pickedHero] = 1

        for pickedHero in dire:
            heroArray[pickedHero] = -1
        prediction = self.model([heroArray])
        return None

    def create_model(self):
        model = keras.Sequential([
            # input layer, heroLength is currently 119
            keras.layers.Dense(self.heroLength, input_shape=(self.heroLength,)),
            # this is the hidden layer
            keras.layers.Dense(300, activation="sigmoid"),
            # Output layer which gives a predicted probability of Radiant win by 0-1.
            keras.layers.Dense(1, activation="sigmoid")
        ])

        model.compile(optimizer="adam", metrics=["accuracy"], loss="hinge")
        return model


if __name__ == '__main__':
    m = Main()
    m.main()

    # Create an instance of the model
