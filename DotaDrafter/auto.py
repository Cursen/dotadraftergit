from Firebase import Firebase
from Match import Match
from scraping import DotaBuff
import StratzDotaAPI


class Main:
    # TODO implement version check, so it runs a check for new heroes in db when a new patch is found. This gotta be run before any matches is attempted to be inserted.
    matches = []
    firebasedb = Firebase.FirebaseTools()

    def __init__(self):
        self.scraper = DotaBuff.Main()
        self.matches = []

    def main(self):
        self.matches = []
        for matchToGet in self.scrape():
            match = Match(StratzDotaAPI.get_match(matchToGet.matchId))
            self.firebasedb.addMatch(match)
    def scrape(self):
        self.scraper.main()
        return self.scraper.getmatchtable()


if __name__ == '__main__':
    m = Main()
    m.main()
