import time
import FireBaseAPIV as Firebase
import StratzDotaAPI as API
from Match import Match
import random


class Main:

    def __init__(self):
        self.fdb = Firebase.Main()
        self.gMatches = []
        self.matches = []

    def main(self):
        self.getMatches()
        self.resetScrapedMatches()
        if len(self.gMatches) > 0:
            self.getMatches()

    def getMatches(self):
        self.resetScrapedMatches()
        idsToSearch = []
        for key, value in self.matches.items():
            if not value['downloaded']:
                idsToSearch.append(key)
        random.shuffle(idsToSearch)
        for id in idsToSearch:
            match = API.get_match(id, 0)
            if match:
                self.gMatches.append(Match(match))
            if len(self.gMatches) > 50:
                self.storeMatches()
            time.sleep(0.5)
        self.storeMatches()

    def getMatchesByArray(self):
        self.resetScrapedMatches()
        idsToSearch = []
        for key, value in self.matches.items():
            if not value['downloaded']:
                idsToSearch.append(key)
        random.shuffle(idsToSearch)
        counter = 0
        idArray = []
        for id in idsToSearch:
            if counter < 10:
                idArray.append(id)
                counter = counter + 1
            elif counter >= 10:
                matches = API.get_matchByArray(idArray, 0)
                for match in matches:

                    if match:
                        self.gMatches.append(Match(match))
                        self.storeMatches()
                idArray.clear()
                counter = 0
        self.storeMatches()

    def storeMatches(self):
        for match in self.gMatches:
            self.fdb.firebase_tools.addMatch(match)
            self.gMatches.remove(match)

    def resetScrapedMatches(self):
        self.matches = self.fdb.firebase_tools.getScrapedMatches()


if __name__ == '__main__':
    m = Main()
    m.main()
