import time
import requests

# find all matches using OpenDota API. Call DotaBuff to get list of IDs.
from requests.exceptions import ChunkedEncodingError
from requests.exceptions import ConnectionError

api_url = 'https://api.stratz.com/api/v1/'
headers = {'Content-Type': 'application/json'}


def get_heroes():
    api_call = '{0}Hero'.format(api_url)

    with requests.get(api_call) as response:

        if response.status_code == 200:
            return response.content
        else:
            print("heroes not gotten")
            return response.raise_for_status()


def get_version():
    api_call = '{0}GameVersion'.format(api_url)

    with requests.get(api_call) as response:

        if response.status_code == 200:
            return response.content
        else:
            print("gameversion not gotten")
            return response.raise_for_status()


def get_match(match_id, counter):
    api_call = '{0}match/{1}/breakdown'.format(api_url, match_id)
    try:
        response = requests.get(api_call)

        if response.status_code == 200:
            print(response.content)
            return response.content
        elif response.status_code == 429:
            print("waiting: {0}".format(response.headers.get('Retry-After')))
            time.sleep(int(response.headers.get('Retry-After')))
        elif counter < 3:
            time.sleep(1)
            print("failed, attempting again: {0}".format(counter))
            counter += 1
            get_match(match_id, counter)
        else:
            print("failed and counter overstepped.")
            return
    except ChunkedEncodingError:
        print("api call did not answer correctly, and gave some chunk encoding issue for: {0}".format(match_id))
    except ConnectionError:
        print("Connection error was raised for: {0}".format(match_id))

def get_matchByArray(match_ids, counter):
    api_call = '{0}match/{1}'.format(api_url, match_ids)
    try:
        response = requests.get(api_call)
        print(response.status_code)
        if response.status_code == 200:
            print(response.content)
            return response.content
        elif response.status_code == 429:
            print("waiting: {0}".format(response.headers.get('Retry-After')))
            time.sleep(int(response.headers.get('Retry-After')))
        elif counter < 3:
            time.sleep(1)
            print("failed, attempting again: {0}".format(counter))
            counter += 1
            get_match(match_ids, counter)
        else:
            print("failed and counter overstepped.")
            return
    except ChunkedEncodingError:
        print("api call did not answer correctly, and gave some chunk encoding issue")
    except ConnectionError:
        print("Connection error was raised")
